class ApplicationController < ActionController::Base
  before_action :set_locale

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to main_app.root_path, alert: exception.message
  end

  def default_url_options
    { locale: I18n.locale }
  end
  
  private
    def set_locale
      I18n.locale = extract_locale || I18n.default_locale
    end

    def extract_locale
      parsed_locale = params[:locale] || request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/)[0] if request.env['HTTP_ACCEPT_LANGUAGE']
      I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
    end


end
