class PagesController < ApplicationController
  def context
  end

  def join
  end

  def home
    @last_memoirs = Memoir.order(created_at: :desc).first(5)
  end

  def about
  end

  def Laboratorio
  end

end
