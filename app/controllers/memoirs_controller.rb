class MemoirsController < ApplicationController
  before_action :set_memoir, only: [:show, :edit, :update, :destroy]

  def index
    @memoirs = Memoir.all
  end

  def show
    @memoir = Memoir.find(params[:id])
  end

  # GET /memoirs/new
  def new
    @memoir = Memoir.new
    @memoir.build_area
  end

  # GET /memoirs/1/edit
  def edit
  end

  # POST /memoirs
  def create
    @memoir = Memoir.new(memoir_params)
    if @memoir.save
      redirect_to memoir_path(@memoir), notice: 'Memoir was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /memoirs/1
  def update
    if @memoir.update(memoir_params)
      redirect_to memoir_path(@memoir), notice: 'Memoir was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /memoirs/1
  def destroy
    @memoir.destroy
    redirect_to memoirs_url, notice: 'Memoir was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_memoir
      @memoir = Memoir.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def memoir_params
      params.require(:memoir).permit(:title, :image, :user_id, :summary, :description, :post, :vimeo_link, :youtube_link, area_attributes: [:latitude, :longitude, :name])
    end

end
