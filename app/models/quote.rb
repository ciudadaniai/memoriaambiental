class Quote < ApplicationRecord
  translates :who, :content
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_one_attached :avatar
  belongs_to :memoir


end
