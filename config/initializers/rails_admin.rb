RailsAdmin.config do |config|

  ### Popular gems integration
  config.parent_controller = 'ApplicationController'

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == CancanCan ==
  config.authorize_with :cancancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.included_models = [
  'User',
  'Memoir',
  'Audio',
  'Memoir::Translation',
  'Area',
  'Area::Translation',
  'Resource',
  'Resource::Translation',
  'Quote',
  'Quote::Translation',
  ]

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  ### Locale Visibility and UX ###

  config.model 'Memoir' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :user
      field :title
      field :summary
    end
  end

  config.model 'Memoir::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :summary, :description, :report
  end

  config.model 'Area' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :name
      field :latitude
      field :longitude
    end
  end

  config.model 'Area::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :name
  end

  config.model 'Resource' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :title
    end
  end

  config.model 'Resource::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :description
  end
  config.model 'Quote' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :content
      field :who
    end
  end

  config.model 'Quote::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :content, :who
  end

end
