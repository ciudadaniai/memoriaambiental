class AddMemoirToResources < ActiveRecord::Migration[6.0]
  def change
    add_reference :resources, :memoir, foreign_key: true
  end
end
