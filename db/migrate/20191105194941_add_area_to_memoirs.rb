class AddAreaToMemoirs < ActiveRecord::Migration[6.0]
  def change
    add_reference :memoirs, :area, foreign_key: true
  end
end
