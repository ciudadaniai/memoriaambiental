class AddPresentationToMemoir < ActiveRecord::Migration[6.0]
  def change
    add_column :memoirs, :presentation, :string
  end
end
