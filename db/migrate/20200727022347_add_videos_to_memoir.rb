class AddVideosToMemoir < ActiveRecord::Migration[6.0]
  def change
    add_column :memoirs, :vimeo_link, :string
    add_column :memoirs, :youtube_link, :string
  end
end
