class AddTranslationtoQuote < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Quote.create_translation_table! :content => :text, :who => :string
      end

      dir.down do
        Quote.drop_translation_table!
      end
    end
  end
end
