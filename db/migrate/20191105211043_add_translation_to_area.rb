class AddTranslationToArea < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Area.create_translation_table! :name => :string
      end

      dir.down do
        Area.drop_translation_table!
      end
    end
  end
end
