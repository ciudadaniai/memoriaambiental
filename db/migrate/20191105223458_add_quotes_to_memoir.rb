class AddQuotesToMemoir < ActiveRecord::Migration[6.0]
  def change
    add_reference :quotes, :memoir, foreign_key: true
  end
end
