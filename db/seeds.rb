# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


require 'faker'

p 'Create users 👩‍💻'

u1 = User.new
u1.email = "admin@a.org"
u1.password = "admina"
u1.save
u1.add_role "admin"

u2 = User.new
u2.email = "editor@a.org"
u2.password = "editora"
u2.save
u2.add_role "editor"

u3 = User.new
u3.email = "author@a.org"
u3.password = "authora"
u3.save
u3.add_role "author"

p 'Users created ✔'


a = Area.create(name: "Hojancha, Costa Rica", latitude: -85.4024586, longitude: 10.0119008,)

p 'Create memories'

m = Memoir.find_or_create_by(
  title: "Nosara, el río que volvió: Memoria Ambiental en Hojancha, Costa Rica",
  summary: "En el cantón de Hojancha, Costa Rica, se encuentra el Río Nosara, que ha sido testigo de una excepcional historia de degradación ambiental y posterior renacimiento debido a un continuo trabajo comunitario que se relata a continuación.",
  description: "En el cantón de Hojancha, Costa Rica, se encuentra el Río Nosara, que ha sido testigo de una excepcional historia de degradación ambiental y posterior renacimiento debido a un continuo trabajo comunitario que se relata a continuación.",
  area: Area.first,
  report: "###¿Cómo afrontar el cambio? ",
  )
path_image = '/public/seeds/rionosara.jpg'
m.image.attach(io: File.open(File.join(Rails.root, path_image)), filename: m.title)

I18n.locale = :pt
m.update(
  title: "Nosara, o rio que voltou: memória ambiental de Hojancha, Costa Rica",
  summary: "No município de Hojancha, na Costa Rica, existe o rio Nosara que testemunhou uma história excepcional de degradação ambiental e subseqüente renascimento,  devido ao trabalho contínuo da comunidade que será descrito abaixo.",
  description: "No município de Hojancha, na Costa Rica, existe o rio Nosara que testemunhou uma história excepcional de degradação ambiental e subseqüente renascimento,  devido ao trabalho contínuo da comunidade que será descrito abaixo.",
  area: Area.first,
  report: "###¿Cómo afrontar el cambio? ",
)

I18n.locale = :es

5.times do |n|
  m = Memoir.create(
    title: 'Titulo de una memoira',
    summary: "Resumen de una memoira. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    area: Area.first,
    report: "###¿Cómo afrontar el cambio?",
    )
  path_image = '/public/seeds/memoir1.jpg'
  m.image.attach(io: File.open(File.join(Rails.root, path_image)), filename: m.title)
end


p 'Memoir created ✔'


p 'Create resources 📷'

I18n.locale = :es
r1 = Resource.find_or_create_by(
  title: "La experiencia forestal de hojancha",
  description: "Más de 35 años de restauración forestal, desarrollo territorial y fortalecimiento social.",
  memoir: Memoir.first,
  link: 'http://repositorio.bibliotecaorton.catie.ac.cr/bitstream/handle/11554/8671/La_experiencia_forestal_de_hojancha.pdf?sequence=1&isAllowed=y',
)
I18n.locale = :pt
r1.update(
  title: "A experiência florestal de Hojancha",
  description: "Mais de 35 anos de restauração florestal, desenvolvimento territorial e fortalecimento social.",
)

I18n.locale = :es
r2 = Resource.find_or_create_by(
  title: "Liderazgo comunitario con compromiso ambiental",
  description: "Fundación Pro Reserva Forestal Monte Alto, cuenca alta del río Nosara, Hojancha, Guanacaste, Costa Rica: Liderazgo comunitario con compromiso ambiental: el diferencial de la Reserva Natural Monte Alto",
  memoir: Memoir.first,
  link: 'http://www.fao.org/forestry/19352-0ed48345bd4ddee7164596d61f6ecc6b0.pdf',
)
I18n.locale = :pt
r2.update(
  title: "Liderança comunitária com compromisso ambiental",
  description: "Fundação Pro Reserva Floresta Monte Alto, bacía do Rio Nosara, Hojancha, Guanacaste, Costa Rica: “ Liderança comunitária com compromisso ambiental: O diferencial da Reserva Natural Monte Alto”",
)

I18n.locale = :es
r3 = Resource.find_or_create_by(
  title: "Restauración del paisaje en Hojancha, Costa Rica",
  description: "",
  memoir: Memoir.first,
  link: 'https://www.researchgate.net/publication/283266290_Restauracion_del_paisaje_en_Hojancha_Costa_Rica'
)
I18n.locale = :pt
r3.update(
  title: "Restauração da paisagem em Hojancha, Costa Rica",
  description: "",
)

I18n.locale = :es
r4 = Resource.find_or_create_by(
  title: "La hazaña de Hojancha, el pueblo que perdió su bosque",
  description: "",
  memoir: Memoir.first,
  link: 'https://vozdeguanacaste.com/la-hazana-de-hojancha-el-pueblo-que-perdio-su-bosque/ '
)
I18n.locale = :pt
r4.update(
  title: "O feito de Hojancha, a cidade que perdeu sua floresta",
  description: "",
)

I18n.locale = :es
r5 = Resource.find_or_create_by(
  title: "La iniciativa cantonal que revivió los bosques de Hojancha",
  description: "",
  memoir: Memoir.first,
  link: 'https://www.youtube.com/watch?v=vn0izVzbJsQ'
)
I18n.locale = :pt
r5.update(
  title: "A iniciativa estadual que reviveu as florestas de Hojancha, Costa Rica",
  description: "",
)

I18n.locale = :es
r6 = Resource.find_or_create_by(
  title: "Hojancha: cuna de la reforestación nacional",
  description: "Por el Programa Producción y Conservación en Bosques del CATIE (Centro Agronómico Tropical de Investigación y Enseñanza).",
  memoir: Memoir.first,
  link: 'https://www.youtube.com/watch?v=iW33uEgQWio'
)
I18n.locale = :pt
r6.update(
  title: "Hojancha: berço do reflorestamento nacional",
  description: "pelo Programa de Produção e Conservação Florestal do CATIE (Centro de Ensino e Pesquisa Agropecuária Tropical)",
)


p 'Resources created ✔'

p 'Create quotes'

I18n.locale = :es

q1 = Quote.find_or_create_by(
  content: "Ser Hojancheño significa proteger Monte Alto y proteger el Río Nosara para tener la fuente de vida, la fuente de agua… significa pasar ese mensaje, esa memoria  a futuras generaciones...es como el cuento que cuentan los abuelos y los padres a sus hijos para tener siempre presente donde vinieron." ,
  who: "Lubica Bogantes. Gestora de Proyectos Ambientales, FUNDECODES.",
  memoir: Memoir.first,
)

I18n.locale = :pt
q1.update(
  content: "Ser de Hojancha significa proteger Monte Alto e o rio Nosara para ter a fonte da vida, a fonte de água… Significa passar essa mensagem, essa memória a futuras gerações... É como a história que avós e pais contam aos seus filhos para sempre lembrarem-se de onde eles vieram." ,
  who: "Lubica Bogantes. Gerente de Projetos Ambientais, FUNDECODES.",
)
path_image = '/public/seeds/av3.png'
q1.avatar.attach(io: File.open(File.join(Rails.root, path_image)), filename: 'Foto de Lubika')


I18n.locale = :es

q2 = Quote.create(
  content: "En Hojancha existen los incentivos económicos para regenerar el bosque y el río, pero el recurso más valioso otorgado por parte de las instituciones ha sido la educación, la concienciación ambiental." ,
  who: "José León Vargas. Ganadero y maestro jubilado.",
  memoir: Memoir.first,
)

I18n.locale = :pt
q2.update(
  content: "Em Hojancha existem incentivos econômicos para restaurar a floresta e o rio, mas o recurso mais valioso concedido pelas instituições tem sido a educação, a conscientização ambiental." ,
  who: "José León Vargas. Ganadero e professor aposentado.",
)
path_image = '/public/seeds/av2.png'
q2.avatar.attach(io: File.open(File.join(Rails.root, path_image)), filename: 'Foto de José Luis Vargas.')




I18n.locale = :es

q3 = Quote.create(
  content: "Hago mi artesanía, principalmente pulseras, collares, aretes o  atrapasueños, a partir de semillas que recojo en la naturaleza. Creo que con esta labor aporto conocimiento en la comunidad sobre estas semillas presentes en nuestro entorno natural.",
  who: "Shirleny Quirós Hernández. Artesana, Artesanías Guapinol",
  memoir: Memoir.first,
)

I18n.locale = :pt
q3.update(
  content: "Faço meu artesanato, principalmente pulseiras, colares, brincos ou filtro dos sonhos, a partir de sementes que coleciono na natureza. Penso que, com este trabalho, trago conhecimento à comunidade sobre essas sementes presentes em nosso ambiente natural." ,
  who: "Shirleny Quirós Hernández. Artesã, Artesanatos Guanipol",
)
path_image = '/public/seeds/av1.png'
q3.avatar.attach(io: File.open(File.join(Rails.root, path_image)), filename: 'Foto de Shirleny Quirós Hernández')



p 'Quotes created ✔'

I18n.locale = :es
